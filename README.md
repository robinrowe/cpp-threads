# cpp-threads: C++ pthreads and promises

Copyright 2024/1/5 Robin Rowe
License MIT open source

## Build

	mkdir build
	cd build
	cmake .. -A x64

## Testing

	cpp_threads/build
	$ ctest --verbose
	UpdateCTestConfiguration  from :C:/code/cpp_threads/build/DartConfiguration.tcl
	UpdateCTestConfiguration  from :C:/code/cpp_threads/build/DartConfiguration.tcl
	Test project C:/code/cpp_threads/build
	Constructing a list of tests
	Done constructing a list of tests
	Updating test list for fixtures
	Added 0 tests to meet fixture requirements
	Checking test dependency graph...
	Checking test dependency graph end
	test 1
		Start 1: test_pthread

	1: Test command: C:\code\cpp_threads\build\Debug\test_pthread.exe
	1: Working Directory: C:/code/cpp_threads/build
	1: Test timeout computed to be: 10000000
	1: Creating thread
	1: Printing Hello World from thread
	1: Thread finished
	1:
	1/4 Test #1: test_pthread .....................   Passed    1.04 sec
	test 2
		Start 2: test_thread

	2: Test command: C:\code\cpp_threads\build\Debug\test_thread.exe
	2: Working Directory: C:/code/cpp_threads/build
	2: Test timeout computed to be: 10000000
	2: starting first helper...
	2: starting second helper...
	2: waiting for helpers to finish...
	2: done!
	2/4 Test #2: test_thread ......................   Passed    1.03 sec
	test 3
		Start 3: test_promise

	3: Test command: C:\code\cpp_threads\build\Debug\test_promise.exe
	3: Working Directory: C:/code/cpp_threads/build
	3: Test timeout computed to be: 10000000
	3: result=21
	3/4 Test #3: test_promise .....................   Passed    1.03 sec
	test 4
		Start 4: test_packaged_task

	4: Test command: C:\code\cpp_threads\build\Debug\test_packaged_task.exe
	4: Working Directory: C:/code/cpp_threads/build
	4: Test timeout computed to be: 10000000
	4: task_lambda: 512
	4: task_bind:   2048
	4: task_thread: 1024
	4/4 Test #4: test_packaged_task ...............   Passed    0.02 sec

	100% tests passed, 0 tests failed out of 4

	Total Test time (real) =   3.13 sec

