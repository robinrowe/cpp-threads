// test_pthread.cpp 
// Created by Robin Rowe 2024-01-05
// MIT open source

#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>  
#include <pthread.h> 
  
void* foo(void *arg) 
{   sleep(1); 
    printf("Printing %s from thread\n",(const char*) arg); 
    return 0;
} 
   
int main() 
{   pthread_t thread_id; 
    puts("Creating thread"); 
    pthread_create(&thread_id, NULL, foo, (void*) "Hello World"); 
    pthread_join(thread_id, NULL);
    puts("Thread finished\n"); 
    return 0; 
}

